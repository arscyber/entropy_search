#ArsCyber software trademark.
#You're free to use this code both commercially and non-commercially.
#Early alpha version. Only works with the most basic operations. Enlarge the Karnaugh map or logical matrices to get a desired result.
#The goal is to predict the maximum information entropy in the code.

#The purposes may be vast, especially in cryptography and business intelligence.

import statistics

#I hope you get the idea. Add larger Karnaugh maps to predict the increases in information entropy better.  
matrix = [
  
[[1, 0], [0, 1]],   #a xnor gate 50% enntropy
[[0, 1], [1, 0]],   #a xor gate 50% entropy
[[0, 0], [0, 1]],   #and gate 25% entropy
[[0, 1], [1, 1]],   #or gate 75% entropy
[[1, 1], [0, 1]]    #implies gate 75% entropy
]


#Instructions go here in their encoded form, they encode entropy increases after every logical operation.
#Why bother going the long way and storing everything in a buffer when you can just index it and revert when needed?
instructionBuffer = []
instructionBuffer.append(3)
instructionBuffer.append(4)
instructionBuffer.append(2)
instructionBuffer.append(2)

#base 5 numbers to a matrix value
def code(index):
    return matrix[index]

def reverseCode(code):
    return matrix.index(code)

print(code(statistics.mode(instructionBuffer))) #Here we get the Karnaugh value, which shows us the peak entropy present in the code.
print(reverseCode(code(statistics.mode(instructionBuffer)))) #Here we get the correspoding base 5 code back. It's base 5 because this is a list of 5 possible logical operations.
#Add different and larger types of Karnaugh maps / logical matrices if you wish.
#In reality, mode is perhaps not the best way of predicting the future entropy. Artificial neural networks work better. But it still remains a good method.
